import java.util.Scanner;

/**
 * Created by amen on 8/21/17.
 */
public class StrategyStdIn implements IInputStrategy {

    private Scanner scanner = new Scanner(System.in);


    public StrategyStdIn() {
    }

    @Override
    public int getInt() {
        System.out.print("Write number: ");
        return scanner.nextInt();
    }

    @Override
    public double getDouble() {
        System.out.print("Write double: ");
        return scanner.nextDouble();
    }

    @Override
    public String getString() {
        System.out.println("Write new text: ");
        return scanner.nextLine();
    }
}
