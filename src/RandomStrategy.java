import java.util.Random;

/**
 * Created by amen on 8/21/17.
 */
public class RandomStrategy implements IInputStrategy {


    public RandomStrategy() {
    }

    @Override
    public String getString() {
        int random = random(90);
        String randomString = String.valueOf(random);
        return randomString;

    }

    @Override
    public double getDouble() {
        return random(100)/random(100);
    }

    @Override
    public int getInt() {
        return random(100);
    }
    protected static int random(int integer) {
        Random random = new Random();
        return random.nextInt(integer);
    }
}
